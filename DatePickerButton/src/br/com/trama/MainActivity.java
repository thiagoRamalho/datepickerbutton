package br.com.trama;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import br.com.trama.datepicker.DatePickerButton;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.container);
		
		layout.addView(new DatePickerButton(this));
	}
}
