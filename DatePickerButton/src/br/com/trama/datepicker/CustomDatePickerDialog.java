package br.com.trama.datepicker;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;

public class CustomDatePickerDialog extends DatePickerDialog{

	/**
	 * set day actual
	 * 
	 * @param context
	 * @param callBack
	 */
	public CustomDatePickerDialog(Context context, OnDateSetListener callBack) {
		this(context, callBack, getYear(), getMonth(), getDay());
	}

	public CustomDatePickerDialog(Context context, OnDateSetListener callBack,
			int year, int monthOfYear, int dayOfMonth) {
		super(context, callBack, year, monthOfYear, dayOfMonth);
	}

	private static int getYear(){
		return Calendar.getInstance().get(Calendar.YEAR);
	}

	private static int getMonth(){
		return Calendar.getInstance().get(Calendar.MONTH);
	}

	private static int getDay(){
		return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}
}