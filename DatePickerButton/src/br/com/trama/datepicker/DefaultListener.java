package br.com.trama.datepicker;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;

public class DefaultListener implements OnClickListener, OnDateSetListener{

	DatePickerDialog dpdlg;
	
	
	public DefaultListener(){
		this(null);
	}
	
	public DefaultListener(DatePickerDialog dpdlg) {
		super();
		this.dpdlg = dpdlg;
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		
	}

	@Override
	public void onClick(View v) {
		
		if(this.dpdlg == null){
			dpdlg = new CustomDatePickerDialog(v.getContext(), this);
		}

		dpdlg.show();
	}

	
}
