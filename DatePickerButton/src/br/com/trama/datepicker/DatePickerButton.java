package br.com.trama.datepicker;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class DatePickerButton extends LinearLayout{

	private EditText editText;
	private ImageButton dateButton;

	public DatePickerButton(Context context) {

		super(context);

		setOrientation(LinearLayout.HORIZONTAL);

		editText = new EditText(context);
		editText.setLayoutParams(getParams(1));
		editText.setFocusable(false);
		editText.setFocusableInTouchMode(false);
		
		Calendar c = Calendar.getInstance();
		setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		
		dateButton = new ImageButton(context);
		dateButton.setLayoutParams(getParams(0));
		dateButton.setBackgroundResource(android.R.drawable.ic_menu_today);

		dateButton.setOnClickListener(new DefaultListener());

		addView(editText);
		addView(dateButton);
	}

	private LinearLayout.LayoutParams getParams(float weight) {
		return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT,weight);
	}

	private void setDate(int year, int monthOfYear, int dayOfMonth) {
		editText.setText(complete(dayOfMonth) +"/"+ complete(++monthOfYear) + "/" + complete(year));
	}

	private String complete(int value){
		return value < 10 ? "0"+value : ""+value;
	}

	

	class DefaultListener implements OnClickListener, OnDateSetListener{

		@Override
		public void onClick(View v) {

			DatePickerDialog dpdlg = new CustomDatePickerDialog(v.getContext(), this);
			dpdlg.show();
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			setDate(year, monthOfYear, dayOfMonth);
		}

	}

}
